// ====================================================================================================
//
// Cloud Code for GetRankPlayerLocal, write your code here to customize the GameSparks platform.
//
// For details of the GameSparks Cloud Code API see https://docs.gamesparks.com/
//
// ====================================================================================================
var page = Spark.getData().page;
var country = Spark.getData().country;
require("RankHelper");
var data = GetRankPlayerLocal(country, page);
Spark.setScriptData("rankPlayerLocal", data);