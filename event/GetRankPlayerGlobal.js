// ====================================================================================================
//
// Cloud Code for GetRankPlayerGlobal, write your code here to customize the GameSparks platform.
//
// For details of the GameSparks Cloud Code API see https://docs.gamesparks.com/
//
// ====================================================================================================
var page = Spark.getData().page;
require("RankHelper");
var data = GetRankPlayerGlobal(page);
Spark.setScriptData("rankPlayerGlobal", data);