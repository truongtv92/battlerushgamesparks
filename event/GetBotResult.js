var userID = Spark.getData().userID;
var battleResult = Spark.getData().score;
require("BotHelper");
var response = GetBotResult(battleResult, userID);
Spark.setScriptData("result", response);
Spark.setScriptData("score", battleResult);
// update data user
require("UserAssestHelper");
var homeUser = Spark.getPlayer();
var homeAssest = homeUser.getScriptData("userAssets");
var homeUserData = homeUser.getScriptData("user");
if(battleResult=="0:0"||battleResult=="1:1"||battleResult=="2:2"){ // hoa
    drawData = response.battleDraw;
    var draw1 = drawData.homeDraw;
    homeAssest.draws = drawData.homeDraw;
    var crown = battleResult.split(':');
     require("ChestHelper");
        if(GetAllVictoryChest().length>0){
           homeAssest.crowns +=parseInt(crown[0]);
        }
        else{
            homeAssest.crowns =0;
        }
     
}
else{ // co thang co thua
    winData = response.battleWin;
    loseData =response.battleLose;
    if(winData!=null){ // home is winner
        homeAssest.coins+=winData.coins;
        homeAssest.exp +=winData.exp;
        homeAssest.trophies +=winData.trophies;
        homeAssest.highestTrophies += winData.highestTrophies;
        //homeAssest.crowns += winData.crowns;
        require("ChestHelper");
        if(GetAllVictoryChest().length>0){
            
            homeAssest.crowns +=winData.crowns;
        }
        else{
            homeAssest.crowns =0;
        }
        
        homeAssest.threeCrownWins += winData.threeCrownWins;
        homeAssest.wins += winData.wins;
        if(winData.chest!=null){
            var homeChest =  homeUser.getScriptData("chests");
            homeChest.push(winData.chest);
            homeUser.setScriptData("chests",homeChest);
        }
    }
    if(loseData!=null){
        var crown = battleResult.split("%3A");
        var loserCrown = 0;
        if(crown[0]<crown[1]){
            loserCrown = parseInt(crown[0]);
        }
        else{
            loserCrown = parseInt(crown[1]);
         }
        homeAssest.loses +=loseData.loses;
        homeAssest.trophies -=loseData.trophies;
        require("ChestHelper");
        if(GetAllVictoryChest().length>0){
           homeAssest.crowns +=loserCrown;
        }
        else{
           homeAssest.crowns =0;
        }
        
        //homeAssest.crowns +=loserCrown;
        if(homeAssest.trophies<0){
            homeAssest.trophies =0;
        }
    }
    
}
// result la homeAssest.trophies
require("RankHelper");
UpdateRank(homeAssest.country, homeUserData.displayName, homeAssest.trophies,homeAssest.id);
homeUser.setScriptData("userAssets", homeAssest);