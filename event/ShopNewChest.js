require("ShopHelper");
require("UserAssestHelper");
require("BattleDeckHelper");
//?arena-code=ARENA_0&chest-type=MAGIC
var arena = Spark.getData().arena;
var chestData =  Spark.getData().chest;
var cost = Spark.getData().gem;
IncreaseCurrency(0,-cost);
var chest = JSON.parse(chestData);
//Spark.getLog().debug(chest);
var chestType = chest.chestType;
var coin = chest.coins;
var gem = chest.gems;
IncreaseCurrency(coin,gem);
var cards = chest.cards;
for(var i =0;i<cards.length;i++){
    UpdateCard(cards[i].id, cards[i].number);
}

var newchest = ShopNewChest(arena, chestType);
chest.cards = JSON.stringify(chest.cards);
Spark.setScriptData("oldChest", chest);
Spark.setScriptData("newChest", newchest);