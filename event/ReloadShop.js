var arena = Spark.getData().arena;
var isReloadCard = Spark.getData().reloadCard==1;
var isReloadChest = Spark.getData().reloadChest==1;
var isReloadOffer = Spark.getData().reloadOffer==1;
require("ShopHelper");
var result = {};
if(isReloadCard){
    var data = GetShopCardData(arena);
    result.packCard = data;
}
if(isReloadChest){
    var data2 = GetShopChestData(arena);
    result.packChest = data2;
}
if(isReloadOffer){
    var data3 = GetShopOfferData(arena);
    result.shopOffers = data3;
}
Spark.setScriptData("data", result);