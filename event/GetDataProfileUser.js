// ====================================================================================================
//
// Cloud Code for GetDataProfileUser, write your code here to customize the GameSparks platform.
//
// For details of the GameSparks Cloud Code API see https://docs.gamesparks.com/
//
// ====================================================================================================
var userId = Spark.getData().userId;
require("UserAssestHelper");
var data = GetUserAssetById(userId);
var playerData = Spark.loadPlayer(data);
Spark.setScriptData("UserAsset", playerData.getScriptData("userAssets"));
Spark.setScriptData("BattleDeck", playerData.getScriptData("battleDeck").deck[playerData.getScriptData("battleDeck").currentDeck]);
Spark.setScriptData("CountCardFound", playerData.getScriptData("battleDeck").characterFound.length);
Spark.setScriptData("UserName", playerData.getScriptData("user").displayName);