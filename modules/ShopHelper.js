require("APIHelper");
require("UserHelper");
function GetShopData(arena){
    var url = GetShopServiceURL()+"?current-arena="+arena+"&user_id="+GetAPIID();
    var response = Spark.getHttp(url).get();
     return response.getResponseJson();
}
function GetShopCardData(arena){
    var url = GetShopCardServiceURL()+"?current-arena="+arena;
    var response = Spark.getHttp(url).get();
     return response.getResponseJson();
}
function GetShopChestData(arena){
    var url = GetShopChestServiceURL()+"?current-arena="+arena;
    var response = Spark.getHttp(url).get();
     return response.getResponseJson();
}
function GetShopOfferData(arena){
    var url = GetShopOfferServiceURL()+"?current-arena="+arena;
    var response = Spark.getHttp(url).get();
     return response.getResponseJson();
}
function ShopNewChest(arena,chestType){
    var url = GetNewChestServiceURL()+"?current-arena="+arena+"&chest-type="+chestType;
    var response = Spark.getHttp(url).get();
     return response.getResponseJson();
}