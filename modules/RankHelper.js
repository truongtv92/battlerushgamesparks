
require("APIHelper");
function UpdateRank(country,displayName,trophies,idUser){
    var url = GetRankServiceURL()+"player";
    var obj = {
         "country": country,
         "displayName": displayName,
         "trophies": trophies,
         "userId": idUser
    };
    var response = Spark.getHttp(url).putJson(obj);
        if(response.getResponseCode()!=200){
            Spark.getLog().error(response.getResponseJson());
        }
}

function GetRankPlayerGlobal(page){
    var url = GetRankPlayerServiceURL()+"global?page=" + page;
        
        var response = Spark.getHttp(url).get();
            if(response.getResponseCode()!=200){
                Spark.getLog().error(response.getResponseJson());
            }
            return response.getResponseJson();
}
function GetRankPlayerLocal(country, page){
    var url = GetRankPlayerServiceURL()+"country?country=" + country  + "&page=" + page;
        
        var response = Spark.getHttp(url).get();
            if(response.getResponseCode()!=200){
                Spark.getLog().error(response.getResponseJson());
            }
            return response.getResponseJson();
}