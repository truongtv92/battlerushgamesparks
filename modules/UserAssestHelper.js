//var USER_ASSETS_SERVICE = "http://br-gateway-service.jx-staging.35.242.252.39.nip.io/br-user-assets-service/";
require("APIHelper");
function UpdateCurrency(coin,gem){
   var userAssets = Spark.getPlayer().getScriptData("userAssets");
   userAssets.coins = coin;
   userAssets.gem = gem;
   Spark.getPlayer().setScriptData("userAssets", userAssets);
}
function UpdateArena(arena){
    var userAssets = Spark.getPlayer().getScriptData("userAssets");
    userAssets.currentArena = arena;
    Spark.getPlayer().setScriptData("userAssets", userAssets);
    SaveUserAssest();
}
function GetCurrentArena(){
    var userAssets = Spark.getPlayer().getScriptData("userAssets");
    return userAssets.currentArena;
}
function IncreaseCurrency(coin,gem){
   var userAssets = Spark.getPlayer().getScriptData("userAssets");
   if(userAssets.coins+coin<0||userAssets.gem + gem<0) return false;
   userAssets.coins += coin;
   userAssets.gem += gem;
   Spark.getPlayer().setScriptData("userAssets", userAssets);
   return true;
}
function UpdateChestSlotIndex(value){
    var userAssets = Spark.getPlayer().getScriptData("userAssets");
    userAssets.chestSlotIndex = value;
    Spark.getPlayer().setScriptData("userAssets", userAssets);
}
function UpdateLevel(level,exp){
    var userAssets = Spark.getPlayer().getScriptData("userAssets");
    userAssets.level = level;
    userAssets.exp = exp;
    Spark.getPlayer().setScriptData("userAssets", userAssets);
}
function AddmoreEXP(exp){
    var userAssets = Spark.getPlayer().getScriptData("userAssets");
    userAssets.exp += exp;
    Spark.getPlayer().setScriptData("userAssets", userAssets);
}
function UpdateMaxMapLevel(mapID){
    var userAssets = Spark.getPlayer().getScriptData("userAssets");
    if(mapID==userAssets.mapLevel){
    userAssets.mapLevel ++;
        
    }
    Spark.getPlayer().setScriptData("userAssets", userAssets);
}
function GetUserAsset(){
    return  Spark.getPlayer().getScriptData("userAssets");
}
function IncreaseCrown(value){
    var userAssets = Spark.getPlayer().getScriptData("userAssets");
     //Spark.getLog().debug("userAssets.crowns before= "+userAssets.crowns);
    userAssets.crowns += value;
   // Spark.getLog().debug("value increase = "+value);
   // Spark.getLog().debug("userAssets.crowns after = "+userAssets.crowns);
    if(userAssets.crowns<0) {userAssets.crowns = 0;}
    Spark.getPlayer().setScriptData("userAssets", userAssets);
}
function UpdateTimeCrown(value){
     var userAssets = Spark.getPlayer().getScriptData("userAssets");
    userAssets.timeCrownDailyActive = value;
    Spark.getPlayer().setScriptData("userAssets", userAssets);
}
function SaveUserAssest(){
    var userData = Spark.getPlayer().getScriptData("userAssets");
    var userBaseData = Spark.getPlayer().getScriptData("baseUserAssets");
    
    if(userData!=userBaseData){
        var url = GetUserAssetsServiceURL();
        //Spark.getLog().debug(userData);
        var response = Spark.getHttp(url).putJson(userData);
        if(response.getResponseCode()!=200){
            Spark.getLog().error(response.getResponseJson());
        }
      // Spark.getLog().error(Spark.getPlayer().getPlayerId());
    }
    
}
function GetUserAssetById(userId){
    var url = GetUserServiceURL() + userId;
    var response = Spark.getHttp(url).get();
    var sparkId = response.getResponseJson().sparkId;
    return  sparkId;
}