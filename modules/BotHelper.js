
require("APIHelper");
function GetBot(deckID,userID,mustWin,arena){
    var url = GetBotSeriveURL() + "robo?current-arena="+arena+"&current-deck="+deckID+"&must-win="+mustWin+"&user-id="+userID;
var responses = Spark.getHttp(url).get();
return responses.getResponseJson();
}

function GetBotResult(result,userID){
    var url = GetBotResultServiceURL()+"?score="+result+"&user-id="+userID;
    //Spark.getLog().debug(url);
     var response = Spark.getHttp(url).postJson({});
        if(response.getResponseCode()!=200){
            Spark.getLog().error(response.getResponseJson());
        }
        
    return response.getResponseJson();
}